import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// element plus
import installElementPlus, { Size } from './plugins/element'
// 初始化css 重置css默认样式
import 'normalize.css/normalize.css'
// 全局 css
import '@/styles/index.scss'
// svg icons
import initSvgIcon from '@/icons/index'
// 挂载到vue实例上
import { ElMessageBox, ElMessage, ElNotification } from 'element-plus'

createApp(App)
  .use(store)
  .use(router)
  .use(installElementPlus)
  .use(initSvgIcon)
  .mount('#app')

/**
 * 相关issue问题
 * Why not on the d.ts use it ?
   (为什么不能在shims-d.ts 中设置这个？
 * https://github.com/vuejs/vue-next/pull/982
 */
// 挂载到vue实例上
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $message: typeof ElMessage;
    $notify: typeof ElNotification;
    $confirm: typeof ElMessageBox.confirm;
    $alert: typeof ElMessageBox.alert;
    $prompt: typeof ElMessageBox.prompt;
    $ELEMENT: {
      size: Size;
    };
  }
}
