# vue3-element-admin

### Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

--------------------------------------------------------

### 规范git commit message
#### type(必须):
- feat：新功能（feature）
- ci：自动化流程配置修改
- fix：修补bug
- docs：文档更新（documentation）
- style：修改了空格、缩进等（不影响代码运行的变动）
- refactor：功能重构（即不是新增功能，也不是修改bug的代码变动）
- test：增加测试
- chore：构建过程或辅助工具的变动 比如 webpack babel eslint配置
- perf：优化相关，比如提升性能、体验。
- revert：回滚
#### scope（可选）:
scope用于说明 commit 影响的范围，比如 feat(plugins/element):  element按需导入
#### subject(必须):
commit简短描述
```sh
git commit -m <type>(<scope>?): <subject>
# example
git commit -m "some message" # fails
git commit -m "fix: some message" # passes
```